package com.example.lenovo.proreminder;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;

public class LoginActivity extends AppCompatActivity {

    private EditText etPassword;
    private Button btnLogin;
    private CheckBox chkPassword;

    private static String PASSWORD = "ProReminder#@!";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        initialiseUIElements();
        addEventListeners();
    }

    private void initialiseUIElements() {
        etPassword = (EditText) findViewById(R.id.et_password);
        btnLogin = (Button) findViewById(R.id.bt_ogin);
        chkPassword = (CheckBox) findViewById(R.id.cb_pwd);
        btnLogin.setEnabled(false);
    }

    private void addEventListeners() {
        etPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(etPassword.getText().toString().trim().length()==0){
                    btnLogin.setEnabled(false);
                } else {
                    btnLogin.setEnabled(true);
                }
            }
            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        chkPassword.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(!isChecked){
                    etPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
                    etPassword.setSelection(etPassword.getText().length());
                }else{
                    etPassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                    etPassword.setSelection(etPassword.getText().length());
                }
            }
        });

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String password = etPassword.getText().toString();
                if(!password.equals(PASSWORD)){
                    AlertDialog.Builder dlgAlert  = new AlertDialog.Builder(LoginActivity.this);

                    dlgAlert.setMessage("wrong password");
                    dlgAlert.setTitle("Login failed...");
                    dlgAlert.setPositiveButton("OK", null);
                    dlgAlert.setCancelable(true);
                    dlgAlert.create().show();
                    etPassword.setText("");
                }
                else{
                    etPassword.setText("");
                    invokeProReminderActivity();
                }
            }
        });
    }

    private void invokeProReminderActivity() {
        Intent proReminderIntent = new Intent(this, proReminderActivity.class);
        startActivity(proReminderIntent);
    }
}
